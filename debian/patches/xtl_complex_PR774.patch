Subject: [PATCH] Fixed handling of xtl::xcomplex
Origin: https://patch-diff.githubusercontent.com/raw/xtensor-stack/xsimd/pull/774.patch
Forwarded: not-needed
From: Johan Mabille <johan.mabille@gmail.com>

---
 include/xsimd/types/xsimd_api.hpp    | 32 ++++++++++++++++++++++++++++
 include/xsimd/types/xsimd_traits.hpp | 12 +++++++++++
 test/test_batch_complex.cpp          | 12 +++++++++++
 3 files changed, 56 insertions(+)

diff --git a/include/xsimd/types/xsimd_api.hpp b/include/xsimd/types/xsimd_api.hpp
index 149ff9b9d..d532fcca7 100644
--- a/include/xsimd/types/xsimd_api.hpp
+++ b/include/xsimd/types/xsimd_api.hpp
@@ -1033,6 +1033,14 @@ namespace xsimd
         return kernel::load_complex_aligned<A>(ptr, kernel::convert<batch_value_type> {}, A {});
     }
 
+#if XSIMD_ENABLE_XTL_COMPLEX
+    template <class To, class A = default_arch, class From, bool i3ec>
+    inline simd_return_type<xtl::xcomplex<From, From, i3ec>, To> load_as(xtl::xcomplex<From, From, i3ec> const* ptr, aligned_mode) noexcept
+    {
+        return load_as<To>(reinterpret_cast<std::complex<From> const*>(ptr), aligned_mode());
+    }
+#endif
+
     /**
      * @ingroup batch_data_transfer
      *
@@ -1061,6 +1069,14 @@ namespace xsimd
         return kernel::load_complex_unaligned<A>(ptr, kernel::convert<batch_value_type> {}, A {});
     }
 
+#if XSIMD_ENABLE_XTL_COMPLEX
+    template <class To, class A = default_arch, class From, bool i3ec>
+    inline simd_return_type<xtl::xcomplex<From, From, i3ec>, To> load_as(xtl::xcomplex<From, From, i3ec> const* ptr, unaligned_mode) noexcept
+    {
+        return load_as<To>(reinterpret_cast<std::complex<From> const*>(ptr), unaligned_mode());
+    }
+#endif
+
     /**
      * @ingroup batch_data_transfer
      *
@@ -1754,6 +1770,14 @@ namespace xsimd
         kernel::store_complex_aligned(dst, src, A {});
     }
 
+#if XSIMD_ENABLE_XTL_COMPLEX
+    template <class To, class A = default_arch, class From, bool i3ec>
+    inline void store_as(xtl::xcomplex<To, To, i3ec>* dst, batch<std::complex<From>, A> const& src, aligned_mode) noexcept
+    {
+        store_as(reinterpret_cast<std::complex<To>*>(dst), src, aligned_mode());
+    }
+#endif
+
     /**
      * @ingroup batch_data_transfer
      *
@@ -1780,6 +1804,14 @@ namespace xsimd
         kernel::store_complex_unaligned(dst, src, A {});
     }
 
+#if XSIMD_ENABLE_XTL_COMPLEX
+    template <class To, class A = default_arch, class From, bool i3ec>
+    inline void store_as(xtl::xcomplex<To, To, i3ec>* dst, batch<std::complex<From>, A> const& src, unaligned_mode) noexcept
+    {
+        store_as(reinterpret_cast<std::complex<To>*>(dst), src, unaligned_mode());
+    }
+#endif
+
     /**
      * @ingroup batch_data_transfer
      *
diff --git a/include/xsimd/types/xsimd_traits.hpp b/include/xsimd/types/xsimd_traits.hpp
index 891eebf2a..ad8016752 100644
--- a/include/xsimd/types/xsimd_traits.hpp
+++ b/include/xsimd/types/xsimd_traits.hpp
@@ -160,11 +160,23 @@ namespace xsimd
         {
         };
 
+        template <class T1, class T2, bool I3EC, class A>
+        struct simd_return_type_impl<xtl::xcomplex<T1, T1, I3EC>, std::complex<T2>, A>
+            : std::enable_if<simd_condition<T1, T2>::value, batch<std::complex<T2>, A>>
+        {
+        };
+
         template <class T1, class T2, bool I3EC, class A>
         struct simd_return_type_impl<xtl::xcomplex<T1, T1, I3EC>, xtl::xcomplex<T2, T2, I3EC>, A>
             : std::enable_if<simd_condition<T1, T2>::value, batch<std::complex<T2>, A>>
         {
         };
+
+        template <class T1, class T2, bool I3EC, class A>
+        struct simd_return_type_impl<std::complex<T1>, xtl::xcomplex<T2, T2, I3EC>, A>
+            : std::enable_if<simd_condition<T1, T2>::value, batch<std::complex<T2>, A>>
+        {
+        };
 #endif
     }
 
diff --git a/test/test_batch_complex.cpp b/test/test_batch_complex.cpp
index 319cc11d2..4ed4728b2 100644
--- a/test/test_batch_complex.cpp
+++ b/test/test_batch_complex.cpp
@@ -123,6 +123,18 @@ class batch_complex_test : public testing::Test
         std::fill(tmp.begin(), tmp.end(), xtl_value_type(2, 3));
         batch_type b0(xtl_value_type(2, 3));
         EXPECT_EQ(b0, tmp) << print_function_name("batch(value_type)");
+
+        batch_type b1 = xsimd::load_as<xtl_value_type>(tmp.data(), xsimd::aligned_mode());
+        EXPECT_EQ(b1, tmp) << print_function_name("load_as<value_type> aligned");
+
+        batch_type b2 = xsimd::load_as<xtl_value_type>(tmp.data(), xsimd::unaligned_mode());
+        EXPECT_EQ(b2, tmp) << print_function_name("load_as<value_type> unaligned");
+
+        xsimd::store_as(tmp.data(), b1, xsimd::aligned_mode());
+        EXPECT_EQ(b1, tmp) << print_function_name("store_as<value_type> aligned");
+
+        xsimd::store_as(tmp.data(), b2, xsimd::unaligned_mode());
+        EXPECT_EQ(b2, tmp) << print_function_name("store_as<value_type> unaligned");
     }
 #endif
 
